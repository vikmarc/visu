# -*- coding: utf-8 -*-
"""
Created on Mon May 16 13:59:14 2022

@author: vikma
"""
import pandas as pd
import statistics as stat
from datareader import datareader as dr

class calculator:
    def __init__(self):
        pass
    """
    #UNUSED
    def getScatterData(self, data, maxSerNo=50, minSerNo=1, measNo=1):
        index = list()
        for i in range(minSerNo, maxSerNo+1,1):
            index.append(i)
        df = pd.DataFrame(index=index, columns=['precision','accuracy'])
        
        for serNo in range(minSerNo,maxSerNo,1):
            vals = data.getValues(measurementNo=measNo, serialNo=serNo)
            params = data.getParams(measurementNo=measNo, serialNo=serNo, parameter='Distance')
            precisionList = list()
            accuracyList = list()
            for k in range(0,len(vals), 1):
                values = vals.iat[k,0]
                parameter = params[k]
                precisionList.append(self.getPrecision(values))
                accuracyList.append(self.getAccuracy(values, parameter))
            df.at[serNo,'precision'] = min(precisionList)
            df.at[serNo,'accuracy'] = min(accuracyList)
        return df
    #UNUSED
    """
    
    
    def getPrecision(self, data):
        """
        return precision value for given sensor

        Parameters
        ----------
        data : list()
            List of measurements.

        Returns
        -------
        float().

        """
        mean = sum(data)/len(data)
        stDeviation = stat.stdev(data)
        precisionPercent = float(1-(stDeviation/mean))
        return precisionPercent
    
    def getAccuracy(self, data, setval):
        df = pd.DataFrame(data)
        df['diff'] = abs(df[0]-setval)
        df['diff%'] = df['diff']/setval
        acc = df['diff%'].mean()
        accuracyPercent = float(1-acc)
        return accuracyPercent
    
    """
    #####  OBSOLETE #####
    def meanOfStdDevNormed(self, params, maxSerNo, minSerNo, measNo, data):
        sensorStdDev = pd.DataFrame()
        for serNo in range(minSerNo-1,maxSerNo,1):
            vals = data.getValues(measurementNo=measNo, serialNo=serNo)
            stdDevsNormed = list()
            for k in range(0,len(vals), 1):
                values = vals.iat[k,0] #Todo: Norm only on distance
                std = stat.stdev(values)
                std /= params[k]
                stdDevsNormed.append(std)
            sensorStdDev = pd.concat(objs=[sensorStdDev, pd.Series(stdDevsNormed)],
                                     axis='columns', ignore_index=True)
        meanOfStdDevs = sensorStdDev.mean(axis='columns')
        return meanOfStdDevs
    #####  OBSOLETE #####
    """
    
    def getAbsDeviation(self, params, maxSerNo, minSerNo, measNo, data, func1, func2=None):
        """
        apply function func1 and func2 to absolute deviation

        Parameters
        ----------
        params : list()
            List of nominal length values.
        maxSerNo : int()
            highest serial number.
        minSerNo : int()
            lowest serial number.
        measNo : int()
            measurement number.
        data : datareader object
            module object containing measurement data.
        func : function object
            max() or min() allowed

        Returns
        -------
        df : pandas.DataFrame()
            Dataframe containing the absolute deviation manipulated by func1 
            and func2for each measurement, indexed to the Distance measured.

        """
        if (func2 == None):
            func2 = func1
        length = len(data.getValues(measurementNo=measNo, serialNo=minSerNo))
        #if measNo==1:
        df = pd.DataFrame(index=params)
        for k in range(length):
            funcVals = list()
            for serNo in range(minSerNo-1,maxSerNo,1):
                vals = data.getValues(measurementNo=measNo, serialNo=serNo)
                values = pd.DataFrame(vals.iat[k,0])
                if measNo==1:    
                    values['diff'] = values[0]-params[k]
                elif((measNo<4) and (measNo>1)):
                    values['diff'] = values[0]-500
                funcVal = abs(func1(values['diff']))
                funcVals.append(funcVal)
            func2Val = func2(funcVals)
            df.at[params[k],0] = func2Val
        return df
    
    def getStdDev(self, params, maxSerNo, minSerNo, measNo, data, func1):
        """
        apply function func1 to standard deviations over all Sensors and
        indexed by the measurement setpoints
        

        Parameters
        ----------
        params : list()
            List of nominal length values.
        maxSerNo : int()
            highest serial number.
        minSerNo : int()
            lowest serial number.
        measNo : int()
            measurement number.
        data : datareader object
            module object containing measurement data.
        func : function object
            max() or min() allowed

        Returns
        -------
        df : pandas.DataFrame()
            Dataframe containing the standard deviation filtered by func1 for 
            each measurement, indexed by the Distance measured.

        """
        length = len(data.getValues(measurementNo=measNo, serialNo=minSerNo))
        df = pd.DataFrame(index=params)
        for k in range(length):
            funcVals = list()
            for serNo in range(minSerNo-1,maxSerNo,1):
                vals = data.getValues(measurementNo=measNo, serialNo=serNo)
                values = pd.DataFrame(vals.iat[k,0])
                funcVal = stat.stdev(values[0])
                funcVals.append(funcVal)
            func1Val = func1(funcVals)
            df.at[params[k],0] = func1Val
        return df
    
    def getM4PlotData(self, data, measParam1_name, measParam1_val, measParam2_name, 
                      measParam2_val, measParam3_name, measParam3_vals, minSerNo, maxSerNo):
        """
        returns plottable dataframe with information for measurement 4 diagrams

        Parameters
        ----------
        data : datareader class object
            
        measParam1_name : str()
            first parameter name for fixed parameter.
        measParam1_val : int()
            First fixed parameter value.
        measParam2_name : str()
            second parameter name for fixed parameter.
        measParam2_val : int()
            second fixed parameter value.
        measParam3_name : str()
            third parameter name for variable parameter.
        measParam3_vals : list()
            parameters for variable parameter.
        minSerNo : int()
            first serial number.
        maxSerNo : int()
            last serial number.

        Returns
        -------
        pandas.DataFrame().

        """
        allSensors = pd.DataFrame()
        for i in range(minSerNo-1, maxSerNo, 1):
            vals = data.getM4Data(data=data, measParam1_name=measParam1_name, 
    measParam1_val=measParam1_val, measParam2_name=measParam2_name, measParam2=measParam2_val, serNo=i) 
            allSensors = pd.concat(objs=[vals, allSensors], axis=0)  
        differences = list()
        means = list()
        stMeans, meansOfDiff2, stdOfDiff2 = list(), list(), list()
        if measParam3_name == 'Distance':
            distances = data.getM4Params(measurementNo=4, serialNo=1, parameter='Distance')
            for p in distances:
                thisTemp = allSensors.where(allSensors[measParam3_name]==p)
                thisTemp = thisTemp.dropna()
                listOfAllVals = thisTemp['Values'].squeeze()
                listOfMeans = list()
                for sensor in listOfAllVals:
                    #for i in range(0,len(sensor),1):
                    #    sensor[i] -= measParam1_val
                    mittelwert = sum(sensor)/len(sensor)
                    mittelwert -= p
                    listOfMeans.append(abs(mittelwert))
                meanOfMeans = sum(listOfMeans)/len(listOfMeans)
                stMeans.append(stat.stdev(listOfMeans))
                means.append(meanOfMeans)
                listOfDifferences, listOfDiff2 = list(), list()                 #wichtig in dataframe
                for sensor in listOfAllVals:
                    mittelwert = sum(sensor)/len(sensor)
                    listOfDifferences.append(mittelwert-meanOfMeans)
                    
                    standardDev = stat.stdev(sensor)
                    listOfDiff2.append(standardDev)
                    
                differencesStdDev = stat.stdev(listOfDifferences) #wichtig in dataframe
                differences.append(differencesStdDev)
                meansOfDiff2.append(sum(listOfDiff2)/len(listOfDiff2))
                stdOfDiff2.append(stat.stdev(listOfDiff2))
        else:
            for p in measParam3_vals:
                thisTemp = allSensors.where(allSensors[measParam3_name]==p)
                thisTemp = thisTemp.dropna()
                listOfAllVals = thisTemp['Values'].squeeze()
                listOfMeans = list()
                for sensor in listOfAllVals:
                    #for i in range(0,len(sensor),1):
                    #    sensor[i] -= measParam1_val
                    mittelwert = sum(sensor)/len(sensor)
                    mittelwert -= 1000
                    listOfMeans.append(abs(mittelwert))
                meanOfMeans = sum(listOfMeans)/len(listOfMeans)
                stMeans.append(stat.stdev(listOfMeans))
                means.append(meanOfMeans)
                listOfDifferences, listOfDiff2 = list(), list()               #wichtig in dataframe
                for sensor in listOfAllVals:
                    mittelwert = sum(sensor)/len(sensor)
                    listOfDifferences.append(mittelwert-meanOfMeans)
                    
                    standardDev = stat.stdev(sensor)
                    listOfDiff2.append(standardDev)
                    
                differencesStdDev = stat.stdev(listOfDifferences) #wichtig in dataframe
                differences.append(differencesStdDev)
                meansOfDiff2.append(sum(listOfDiff2)/len(listOfDiff2))
                stdOfDiff2.append(stat.stdev(listOfDiff2))
        
            
        plotData = pd.DataFrame({'meanOfMeans':means, 'differencesStdDev':differences,
                                 'stDevs':stMeans, 'meansOfDiff2':meansOfDiff2, 
                                 'stdOfDiff2':stdOfDiff2}, index=measParam3_vals)
        plotData['upper'] = plotData['meanOfMeans'] + (plotData['stDevs']/2)
        #plotData['lower'] = plotData['meanOfMeans'] - (plotData['stDevs']/2)
        plotData['3Sigm_upper'] = plotData['meanOfMeans'] + (plotData['stDevs']*3/2)
        plotData['Precision1Sigma'] = plotData['meansOfDiff2'] + (plotData['stdOfDiff2']/2)
        plotData['Precision3Sigma'] = plotData['meansOfDiff2'] + (plotData['stdOfDiff2']*3/2)
        return plotData