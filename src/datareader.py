# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import os

class datareader:
    def __init__(self, fileName, fileFormat, rootDir):
        """
        

        Parameters
        ----------
        filePath : str
            Filepath relative to executed path.
        fileName : filename of desired file
            Full Filename with ending.
        fileFormat : Format of File to read
            Allowed are .xml, .json, .mat and .csv
        rootDir : Root Directory for data archive
            e.g. "../data/sensor_data/"

        Returns
        -------
        None.

        """
        #self.filePath = filePath
        self.fileFormat = fileFormat
        self.fileName = fileName
        self.rootDir = rootDir
        self.df = list()
    
    def readData(self, filePath=None, fileName=None, fileFormat=None, rootDir=None):
        """
        Reads all json data in rootDir and stores it internally

        Parameters
        ----------
        filePath : str
            Filepath relative to executed path.
        fileName : filename of desired file
            Full Filename with ending.
        fileFormat : Format of File to read
            Allowed are .xml, .json, .mat and .csv
        rootDir : Root Directory for data archive
            e.g. "../data/sensor_data/"

        Returns
        -------
        df : TYPE
            DESCRIPTION.

        """
        if (fileName == None):
            fileName = self.fileName
        else: 
            self.fileName = fileName
            
        if (fileFormat == None):
            fileFormat = self.fileFormat
        else: 
            self.fileFormat = fileFormat
        
        if (rootDir == None):
            rootDir = self.rootDir
        else: 
            self.rootDir = rootDir
            
        df = list()
        try:
            if (fileFormat == ".json"):
                files = self.__fileFinder__(self.fileName)
                for file in files:
                    data = pd.DataFrame(pd.read_json(str(file), 
                        orient='values'))
                    measData = pd.json_normalize(data['Measurement'])
                    data.drop(columns=['Measurement'], inplace=True)
                    data = data.iloc[:1,:]
                    data = pd.concat([data, measData])
                    df.append(data)
        except Exception as e:
            print(e)
        self.df = df

        
    def getValues(self, measurementNo, serialNo):
        """
        Return List of lists containing measurement values for a
        desired measurement and sensor/SerialNumber

        Parameters
        ----------
        measurementNo : int()
            Measurement Number; Allowed are 1 to 4.
        serialNo : int()
            Serial Number of Sensor; Allowed are 1-50.

        Returns
        -------
        container : list()
            List of lists containing measurement values for a
            desired measurement and sensor/SerialNumber.

        """
        df = self.df[serialNo]
        df = df.iloc[1:,:]
        container = pd.DataFrame(np.where(df['TestNumber'] == measurementNo, 
                                          df['Values'], None))
        container.dropna(inplace=True)
        #len(container.iat[0,0])
        return container
    
    def getParams(self, measurementNo, serialNo, parameter):
        """
        Return desired measurement parameter

        Parameters
        ----------
        measurementNo : int()
            Measurement Number; Allowed are 1 to 4.
        serialNo : int()
            Serial Number of Sensor; Allowed are 1-50.
        parameter : str()
            Desired measurement parameter as String; ALlowed are
            'Temperature' 'Light' 'Distance'.

        Returns
        -------
        container : list()
            List of measurement parameter values.

        """
        df = self.df[serialNo-1]
        params = df['Parameter']
        params = params.iloc[1:]
        container = list()
        
        if type(parameter) is not str():
            if parameter == 1:
                parameter = 'Distance'
            elif parameter == 2:
                parameter = 'Temperature'
            elif parameter == 3:
                parameter = 'Light'
        
        for i in range(0,len(params),1):
            for k in range(0, len(params[i]),1):
                if ((params[i])[k])['Type'] == parameter:
                    container.append(((params[i])[k])['Value'])
        l = pd.Series(container)
        if measurementNo == 1:
            end = self.__paramCount__(df, measurementNo)
            container = list(l.iloc[:end])
        elif measurementNo == 2:
            start = self.__paramCount__(df, 1)
            end = start + self.__paramCount__(df, measurementNo)
            container = l.iloc[start:end]
        elif measurementNo == 3:
            start = self.__paramCount__(df, 1) + self.__paramCount__(df, 2)
            end = start + self.__paramCount__(df, measurementNo)
            container = l.iloc[start:end]
        elif measurementNo == 4:
            start = len(l)-self.__paramCount__(df, measurementNo)
            container = l.iloc[start:]
        #container.reset_index()
        container = list(container)
        return container
    
    def getUnit(self, measurementNo, serialNo, parameter):
        """
        Return desired measurement parameter unit

        Parameters
        ----------
        measurementNo : int()
            Measurement Number; Allowed are 1 to 4.
        serialNo : int()
            Serial Number of Sensor; Allowed are 1-50.
        parameter : str()
            Desired measurement parameter as String; ALlowed are
            'Temperature' 'Light' 'Distance'.

        Returns
        -------
        container : str()
            Measurement Unit.

        """
        df = self.df[serialNo-1]
        params = df['Parameter']
        params = params.iloc[1:]
        container = None
        for i in range(0,len(params),1):
            for k in range(0, len(params[i]),1):
                if container is not None:
                    return container
                if ((params[i])[k])['Type'] == parameter:
                    container = (((params[i])[k])['Unit'])
                    
    def getM4Data(self, data, measParam1_name, measParam1_val, measParam2_name, measParam2, serNo):
        v=data.getValues(measurementNo=4, serialNo=serNo)
        v.reset_index(drop=True, inplace=True)
        dist = pd.Series(data.getParams(measurementNo=4, serialNo=serNo, parameter='Distance'))
        light = pd.Series(data.getParams(measurementNo=4, serialNo=serNo, parameter='Light'))
        temp = pd.Series(data.getParams(measurementNo=4, serialNo=serNo, parameter='Temperature'))
        pMeas4 = pd.concat(objs=[dist, light, temp], axis=1)
        pMeas4 = pd.concat([pMeas4,v], axis='columns', ignore_index=True)
        pMeas4.rename(columns = {0:'Distance', 1:'Light', 2:'Temperature', 3:'Values'}, inplace = True)
        

        hundreds = pMeas4.where(((pMeas4[measParam1_name]==measParam1_val) 
                             & (pMeas4[measParam2_name]==measParam2)))
        hundreds = hundreds.dropna()   
        return hundreds
    
    def getM4Params(self, measurementNo, serialNo, parameter):
        p = self.getParams(measurementNo, serialNo, parameter)
        s=pd.Series(p)
        s.drop_duplicates(keep='first', inplace=True)
        return s
    
    def __paramCount__(self, df, measurementNo):
        paramCounts = df['TestNumber'].value_counts(dropna=True)
        paramCounts.sort_index(inplace=True)
        paramCount = paramCounts.iat[measurementNo-1]
        return paramCount
    
    def __fileFinder__(self, fileFormat=None):
        """
        Generates list of files with given format

        Parameters
        ----------
        fileFormat : TYPE, optional
            DESCRIPTION. The default is None.

        Returns
        -------
        filePaths : str()
            File ending, e.g. '.json'.

        """
        filePaths = []
        if fileFormat == None:
            return filePaths
        for root, dirs, files in os.walk(self.rootDir):
            for file in files:
                if file.endswith(self.fileFormat):
                    filepath = os.path.join(root, file)
                    filePaths.append(filepath)
        return filePaths