import pandas as pd
from matplotlib import pyplot as plt
from datareader import datareader as dr
import statistics as stat
from calculator import calculator as calc
import numpy as np

#filepath = str("../data/sensor_data/SN000001/csv_files/Meas#1/data.csv")
filename = str("data.json")
fileformat = str(".json")
#measNo = 3

#serNo = 1
maxSerNo = 50
minSerNo = 1

def mean(listlike):
    mean = sum(listlike)/len(listlike)
    return(mean)

def plotter1(measNo, data):
    """
    Plots absulute deviation plots

    Parameters
    ----------
    measNo : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    if measNo==1:
        param = 'Distance'
    elif measNo==2:
        param = 'Temperature'
    elif measNo==3:
        param = 'Light'
    elif measNo==4:
        param = 'Distance'
    
    print("### Start Collecting Params ###")
    params = data.getParams(measurementNo=measNo, serialNo=1, parameter=param)
    distances = list()
    for i in range(0,len(params),1):
        distances.append(500)
    
    print("### Finished Collecting Params ###")
    calculator = calc()
    unit = data.getUnit(measurementNo=measNo, serialNo=1, parameter=param)
    print("### Start Calculation of stDevs ###")
    stDevs = calculator.getAbsDeviation(params=params, maxSerNo=maxSerNo, minSerNo=minSerNo, measNo=measNo, data=data, func1=mean, func2=stat.stdev)
    #print("### Start Calculation of maxima ###")
    #maxima = calculator.getAbsDeviation(params=params, maxSerNo=maxSerNo, minSerNo=minSerNo, measNo=measNo, data=data, func1=mean, func2=max)
    print("### Start Calculation of mean ###")
    mittelwert = calculator.getAbsDeviation(params=params, maxSerNo=maxSerNo, minSerNo=minSerNo, measNo=measNo, data=data, func1=mean)
    mittelwert['1Sigma'] = mittelwert.iloc[:,0] + ((stDevs.iloc[:,0])/2)
    mittelwert['3Sigma'] = mittelwert.iloc[:,0] + ((stDevs.iloc[:,0])*3/2)
    plt.figure()
    plt.grid(visible=True)
    #plt.axis(xmin=90, xmax=1100, ymin=0, ymax=20)
    plt.plot(mittelwert['1Sigma'], label='Mean deviation 1 sigma')
    plt.plot(mittelwert['3Sigma'], label='Mean deviation 3 sigma')
    plt.plot(mittelwert[0], label='Absolute mean deviation')
    if measNo == 3:
        plt.axhline(y=15, color='r', linestyle='-', label='Max accepted deviation')
    plt.legend()
    plt.ylabel('Deviation from setpoint in mm')
    plt.xlabel(param+' in '+unit)
    plt.title('Accuracy against parameter '+param)
    plt.savefig('../out/measurement_'+str(measNo)+'_absDeviation_new.pdf')
    #hier die 3 anderen Diagramme

def plotter2(measNo, data):
    """
    Plot standard deviation plots

    Parameters
    ----------
    measNo : int()
        Measurement Number.

    Returns
    -------
    None.

    """
    if measNo==1:
        param = 'Distance'
    elif measNo==2:
        param = 'Temperature'
    elif measNo==3:
        param = 'Light'
    elif measNo==4:
        param = 'Distance'
    print("### Start Collecting Params ###")
    params = data.getParams(measurementNo=measNo, serialNo=1, parameter=param)
    distances = list()
    for i in range(0,len(params),1):
        distances.append(500)
    print("### Finished Collecting Params ###")
    calculator = calc()
    
    print("### Start Calculation of mean ###")
    mittelwert = calculator.getStdDev(params=params, maxSerNo=maxSerNo, minSerNo=minSerNo, measNo=measNo, data=data, func1=mean)
    print("### Start Calculation of maxima ###")
    maxima = calculator.getStdDev(params=params, maxSerNo=maxSerNo, minSerNo=minSerNo, measNo=measNo, data=data, func1=max)
    
    unit = data.getUnit(measurementNo=measNo, serialNo=1, parameter=param)
    
    plt.figure()
    plt.grid(visible=True)
    #plt.axis(xmin=90, xmax=1100, ymin=0, ymax=20)
    plt.plot(maxima, label='Maximum standard deviation')
    plt.plot(mittelwert, label='Mean standard deviation')
    if measNo ==2:
        pass
    else:
        plt.axhline(y=25, color='r', linestyle='-', label='Max accepted sigma')
    plt.legend()
    plt.ylabel('Deviation from mean in mm')
    plt.xlabel(param+' in '+unit)
    plt.title('Precision against parameter '+param)
    plt.savefig('../out/measurement_'+str(measNo)+'_stdDev_new.pdf')
    
    
def plotter3(measNo):
    """
    Plots scatter plots

    Parameters
    ----------
    measNo : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    if measNo==1:
        param = 'Distance'
    elif measNo==2:
        param = 'Temperature'
    elif measNo==3:
        param = 'Light'
    elif measNo==4:
        param = 'Distance'

    print("### Start Collecting Params ###")
    params = data.getParams(measurementNo=measNo, serialNo=1, parameter=param)
    distances = list()
    for i in range(0,len(params),1):
        distances.append(500)
    
    print("### Finished Collecting Params ###")
    calculator = calc()
    scatterData = calculator.getScatterData(data=data)
    
    plt.figure()
    #plt.grid(visible=True)
    #plt.axis(xmin=0.85, xmax=1.1, ymin=.85, ymax=1.1)
    plt.ylabel('Precision')
    plt.xlabel('Accuracy')
    plt.title('Average Precision against Accuracy')
    scatter = plt.scatter(x=scatterData['accuracy'], y=scatterData['precision'])
    #plt.axhline(y=1, color='g', linestyle='-.')
    #plt.axvline(x=1, color='g', linestyle='-.')
    #x = np.linspace(.85, 1, 1000)
    #plt.plot(x, -(x-.9)+1, color='r', linestyle='-')
    #plt.show()
    
    plt.savefig('../out/measurement_'+str(measNo)+'_scatter.pdf')
    
if __name__ == "__main__":
    data = dr(filename, fileformat, rootDir="../data/sensor_data")
    print("### Reading Data ###")
    data.readData()
    print("### Finished Reading Data ###")    
    processor = calc()
    
    """
    paramNames = ['Distance', 'Temperature', 'Light']
    t = data.getM4Params(measurementNo=4, serialNo=1, parameter='Temperature')
    d = data.getM4Params(measurementNo=4, serialNo=1, parameter='Distance')
    l = data.getM4Params(measurementNo=4, serialNo=1, parameter='Light')
    for x in paramNames:
        k = None
        if x == 'Distance':
            p = d
            o1 = l
            o1N = 'Light'
            o2 = t
            o2N = 'Temperature'
            k = processor.getM4PlotData(data, 'Temperature', max(t), 'Light', max(l), x, p, minSerNo, maxSerNo)
        elif x == 'Light':
            p = l
            o1 = t
            o1N = 'Temperature'
            o2 = d
            o2N = 'Distance'
            k = processor.getM4PlotData(data, 'Temperature', max(t), 'Distance', max(d), x, p, minSerNo, maxSerNo)
        elif x == 'Temperature':
            p = t
            o1 = l
            o1N = 'Light'
            o2 = d
            o2N = 'Distance'
            k = processor.getM4PlotData(data, 'Light', max(l), 'Distance', max(d), x, p, minSerNo, maxSerNo)
    """
    """
        ############# Plot 1
        plt.figure()
        unit = data.getUnit(4,1,x)
        plt.grid(visible=True)
        plt.plot(k['meanOfMeans'], label='Accuracy')
        plt.plot(k['upper'], label=('Upper sigma'))
        #plt.plot(k['lower'], label=('lower sigma'))
        plt.plot(k['3Sigm_upper'], label=('Upper 3 sigma'))
      
        #plt.axhline(y=15, color='r', linestyle='-', label='Max accepted deviation')
        plt.legend()
        plt.ylabel('Deviation in mm')
        plt.xlabel(x+' in '+unit)
        plt.title('Accuarcy at ' + str(max(o1)) + ' ' + str(data.getUnit(4,1,o1N))
                  +' and ' + str(max(o2)) + ' ' + str(data.getUnit(4,1,o2N)) + ' against parameter '+x)
        plt.savefig('../out/meas_4_' + str(x) + '_accuracy.pdf')
        """
        ################# PLOT 2
    
        plt.figure()
        unit = data.getUnit(4,1,x)
        plt.grid(visible=True)
        plt.plot(k['meansOfDiff2'], label='Precision')
        plt.plot(k['Precision1Sigma'], label=('Upper sigma'))
        #plt.plot(k['lower'], label=('lower sigma'))
        plt.plot(k['Precision3Sigma'], label=('Upper 3 sigma'))
      
        #plt.axhline(y=15, color='r', linestyle='-', label='Max accepted deviation')
        plt.legend()
        plt.ylabel('Deviation in mm')
        plt.xlabel(x+' in '+unit)
        plt.title('Accuarcy at ' + str(max(o1)) + ' ' + str(data.getUnit(4,1,o1N))
                  +' and ' + str(max(o2)) + ' ' + str(data.getUnit(4,1,o2N)) + ' against parameter '+x)
        plt.savefig('../out/meas_4_' + str(x) + '_precision.pdf')
    
    
    for i in range(1,4,1):
        plotter1(i, data)
        plotter2(i, data)
        #plotter3(i, data)
        print(i)
    